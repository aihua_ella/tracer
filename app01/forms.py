import re
import hashlib

from django import forms

from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError

import redis

from app01 import models
from django.conf import settings


class RegForm(forms.ModelForm):
    phone = forms.CharField(
        label="手机号",
        widget=forms.TextInput,
        validators=[RegexValidator(r'^(1[3|4|5|6|7|8|9])\d{9}$', "无效的手机号")])

    password = forms.CharField(label="密码", widget=forms.PasswordInput)

    confirm_password = forms.CharField(label="确认密码", widget=forms.PasswordInput)

    code = forms.CharField(label="验证码")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs["class"] = "form-control"
            field.widget.attrs["placeholder"] = "请输入{}".format(field.label)

    def clean_username(self):
        username = self.cleaned_data.get("username")

        pattern = r'[a-zA-Z0-9_-]{4,16}$'

        if not re.match(pattern, username):
            raise ValidationError("用户名4到16位（字母，数字，下划线，减号）")

        return username

    def clean_password(self):
        password = self.cleaned_data.get("password")

        # pattern = '^.*(?=.{6,})(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*? ]).*$'

        # if not re.match(pattern, password):
        #     raise ValidationError("密码最少6位，包括至少1个大写字母，1个小写字母，1个数字，1个特殊字符")

        return password

    def clean(self):

        # check verification code
        code = self.cleaned_data.get("code", "")
        phone = self.cleaned_data.get("phone")

        conn = redis.Redis(host='127.0.0.1', port=6379, password="wuaihua123")

        if not code.encode() == conn.get(phone):
            self.add_error("code", "验证码错误")
            raise ValidationError("验证码错误")

        # check password and confirm_password
        password = self.cleaned_data.get("password", "")
        confirm_password = self.cleaned_data.get("confirm_password")

        if not password == confirm_password:
            self.add_error("confirm_password", "两次输入的密码不一致")

            raise ValidationError("两次输入的密码不一致")

        m = hashlib.md5()
        m.update(password.encode())

        encrypted_password = m.hexdigest()

        self.cleaned_data["password"] = encrypted_password

        return self.cleaned_data

    class Meta:

        model = models.UserInfo

        fields = [
            "username",
            "email",
            "password",
            "confirm_password",
            "phone",
            "code",
        ]
