import random
import re

from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from django.conf import settings
from utils.tencent.sms import send_sms_single

import redis

from app01 import forms


# Create your views here.

def send_sms(request):
    template_name = request.POST.get("tpl")

    template_id = settings.TENCENT_SMS_TEMPLATES.get(template_name)

    if not template_id:
        return HttpResponse("模板不存在")

    phone = request.POST.get("phone")

    pattern = r'^(1[3|4|5|6|7|8|9])\d{9}$'

    if not re.match(pattern, phone):
        return HttpResponse("无效的手机号码")

    code = random.randrange(1000, 9999)

    res = send_sms_single(phone, template_id, [code])

    if phone:
        conn = redis.Redis(host='127.0.0.1', port=6379, password="wuaihua123")

        conn.set(phone, code, ex=180)

        return HttpResponse("验证码成功发送")

    return HttpResponse(res["errmsg"])


def register(request):
    form_obj = forms.RegForm()

    if request.method == "POST":

        print(request.POST)
        form_obj = forms.RegForm(request.POST)

        if form_obj.is_valid():
            form_obj.save()

            return HttpResponse("注册成功")

    context = {
        "form_obj": form_obj,
    }

    return render(request, "app01/register.html", context)


# def test(request):
# phone = request.POST.get('phone')
# template_name = request.POST.get('tpl')
# template_id = settings.TENCENT_SMS_TEMPLATES.get(template_name)
#
# code = random.randrange(1000, 9999)
#
# res = [phone, template_id, [code]]
# print(res)
#
# if phone:
#     # conn = redis.Redis(**settings.REDIS_ARGS)
#     conn = redis.Redis(host='127.0.0.1', port=6379, password="wuaihua123")
#
#     conn.set(phone, code, ex=60)
#
#     return HttpResponse(f"验证码成功发送{code}")
#     # for testing without sending sms
#     # return HttpResponse(f"验证码: {code}")
#
# return JsonResponse({"status": True, "code": code})


def test(request):
    return render(request, 'app01/test.html')


def cos_credential(request):
    from sts.sts import Sts
    config = {
        # 临时密钥有效时长，单位是秒（30分钟=1800秒）
        'duration_seconds': 1800,
        # 固定密钥 id
        'secret_id': settings.TENCENT_COS_SECRET_ID,
        # 固定密钥 key
        'secret_key': settings.TENCENT_COS_SECRET_KEY,
        # 换成你的 bucket
        'bucket': "thefirst-18201048114-1585046638-1301482361",
        # 换成 bucket 所在地区
        'region': "ap-chengdu",
        # 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的具体路径
        # 例子： a.jpg 或者 a/* 或者 * (使用通配符*存在重大安全风险, 请谨慎评估使用)
        'allow_prefix': '*',
        # 密钥的权限列表。简单上传和分片需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
        'allow_actions': [
            'name/cos:PostObject',
            # 'name/cos:DeleteObject',
            # "name/cos:UploadPart",
            # "name/cos:UploadPartCopy",
            # "name/cos:CompleteMultipartUpload",
            # "name/cos:AbortMultipartUpload",
            "*",
        ],

    }

    sts = Sts(config)
    result_dict = sts.get_credential()
    return JsonResponse(result_dict)
