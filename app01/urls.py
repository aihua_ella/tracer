from django.conf.urls import url

from app01 import views

urlpatterns = [
    url(r'^register/$', views.register, name="register"),
    url(r'^send_sms/$', views.send_sms, name="send_sms"),
    url(r'^test/$', views.test, name="test"),
    url(r'^cos/credential/$', views.cos_credential, name='file'),
]