# coding:utf-8
# @Author:ella

from django.shortcuts import render, reverse, redirect
from web.forms.wiki import WikiModelForm
from web import models
from django.http import JsonResponse
from utils.tencent.cos import upload_file
from utils.encrypt import uid
from django.views.decorators.csrf import csrf_exempt


def wiki(request, project_id):
    wiki_id = request.GET.get('wiki_id')

    if not wiki_id or not wiki_id.isdecimal():
        return render(request, 'wiki.html')

    wiki_object = models.Wiki.objects.filter(id=wiki_id, project_id=project_id).first()

    return render(request, 'wiki.html', {"wiki_object": wiki_object})


def wiki_add(request, project_id):
    if request.method == 'GET':
        form = WikiModelForm(request)
        return render(request, 'wiki_form.html', {"form": form})

    form = WikiModelForm(request, request.POST)

    if form.is_valid():

        if form.instance.parent:
            form.instance.depth = form.instance.parent.depth + 1

        else:
            form.instance.depth = 1

        form.instance.user = request.current_user.user
        form.instance.project = request.current_user.project

        form.save()

        url = reverse('wiki', kwargs={'project_id': project_id})

        return redirect(url)

    return render(request, 'wiki_form.html', {"form": form})


def wiki_edit(request, project_id, wiki_id):
    wiki_object = models.Wiki.objects.filter(project_id=project_id, id=wiki_id).first()
    form = WikiModelForm(request, instance=wiki_object)

    if request.method == "POST":

        form = WikiModelForm(request, request.POST, instance=wiki_object)

        if form.is_valid():
            form.save()

        return redirect(reverse('wiki', args=(request.current_user.project.id,)))

    return render(request, 'wiki_form.html', {"form": form})


def wiki_catalog(request, project_id):
    # data = models.Wiki.objects.filter(project=request.current_user.project).values_list('id', 'title', 'parent_id')
    data = models.Wiki.objects.filter(project=request.current_user.project).values('id', 'title', 'parent_id').order_by(
        'depth', 'id')

    return JsonResponse({'status': True, "data": list(data)})


def wiki_del(request, project_id, wiki_id):
    wiki = models.Wiki.objects.filter(project_id=project_id, id=wiki_id).delete()

    return redirect(reverse("wiki", args=(request.current_user.project.id,)))

@csrf_exempt
def wiki_upload(request, project_id):
    """ markdown插件上传图片 """
    result = {
        'success': 0,
        'message': None,
        'url': None
    }

    image_object = request.FILES.get('editormd-image-file')
    if not image_object:
        result['message'] = "文件不存在"
        return JsonResponse(result)

    ext = image_object.name.rsplit('.')[-1]
    key = "{}.{}".format(uid(request.current_user.user.mobile_phone), ext)
    image_url = upload_file(
        request.current_user.project.bucket,
        request.current_user.project.region,
        image_object,
        key
    )
    result['success'] = 1
    result['url'] = image_url
    return JsonResponse(result)


