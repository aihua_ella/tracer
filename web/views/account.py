# coding:utf-8
# @Author:ella


from django.shortcuts import render, redirect, HttpResponse
from web.forms.account import RegisterModelForm, SendSmsForm, SmsLoginForm, LoginForm
from django.http import JsonResponse
from web import models
from django.db.models import Q
from utils.image_code import check_code
from io import BytesIO
import datetime


def register(request):
    if request.method == 'GET':
        form = RegisterModelForm()
        return render(request, 'register.html', {"form": form})

    form = RegisterModelForm(request.POST)

    if form.is_valid():
        instance = form.save()

        import uuid
        order_id = str(uuid.uuid4())
        policy_object = models.PricePackage.objects.filter(type=1, title="免费版").first()

        models.OrderDetail.objects.create(
            order_id=order_id,
            status=2,
            user=instance,
            price_policy=policy_object,
            num=0,
            actually_paid=0,
            origin_price=0,
            start_time=datetime.datetime.now()
        )

        return JsonResponse({"status": True, 'data': '/login/'})

    return JsonResponse({"status": False, 'error': form.errors})


def send_sms(request):
    """发送短信"""
    form = SendSmsForm(request, data=request.GET)
    # print(request.GET.get('mobile_phone'))

    if form.is_valid():
        return JsonResponse({'status': True})

    return JsonResponse({'status': False, 'error': form.errors})


def login_sms(request):
    if request.method == 'GET':
        form = SmsLoginForm()
        return render(request, 'login_sms.html', {"form": form})

    form = SmsLoginForm(data=request.POST)

    if form.is_valid():
        mobile_phone = form.cleaned_data['mobile_phone']

        user_obj = models.UserInfo.objects.filter(mobile_phone=mobile_phone).first()
        request.session['user_id'] = user_obj.pk
        request.session.set_expiry(60 * 60 * 24 * 14)

        return JsonResponse({'status': True, 'data': '/index/'})

    return JsonResponse({'status': False, 'error': form.errors})


def login(request):
    if request.method == 'GET':
        form = LoginForm(request)
        return render(request, 'login.html', {"form": form})

    form = LoginForm(request, data=request.POST)

    # print(request.POST)

    if form.is_valid():

        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        # print(password)

        user_obj = models.UserInfo.objects.filter(Q(mobile_phone=username) | Q(email=username)).filter(
            password=password).first()

        if user_obj:
            request.session['user_id'] = user_obj.pk
            request.session.set_expiry(60 * 60 * 24 * 14)

            return redirect('index')
        form.add_error('username', '用户名或密码错误')

    return render(request, 'login.html', {'form': form})


def image_code(request):
    image_object, code = check_code()
    print(code)

    request.session['image_code'] = code
    request.session.set_expiry(60)  # 主动修改session的过期时间为60s

    stream = BytesIO()
    image_object.save(stream, 'png')

    return HttpResponse(stream.getvalue())


def logout(request):
    request.session.flush()
    return redirect('index')
