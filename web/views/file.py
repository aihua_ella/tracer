# coding:utf-8
# @Author:ella

from django.shortcuts import render, HttpResponse, reverse
from web.forms.file import FolderModelForm, FileModelForm
from django.http import JsonResponse
from web import models
from django.forms import model_to_dict
from utils.tencent.cos import delete_file, delete_file_list, credential
import json
from django.views.decorators.csrf import csrf_exempt
import requests


def file(request, project_id):
    parent_object = None
    folder_id = request.GET.get('folder')
    if folder_id and folder_id.isdecimal():
        parent_object = models.File.objects.filter(id=int(folder_id), project_id=request.current_user.project.id,
                                                   type=2).first()

    if request.method == 'GET':

        # 导航条/浏览路径
        breadcrumb_trail = []
        parent = parent_object
        # 如果当前有父目录的话，循环找上级目录到根目录为止
        while parent:
            breadcrumb_trail.insert(0, model_to_dict(parent, ['id', 'name']))
            parent = parent.parent

        # 文件列表，获取当前目录下的所有文件 和文件夹
        queryset = models.File.objects.filter(project=request.current_user.project)

        if parent_object:
            file_list = queryset.filter(parent=parent_object).order_by('-type')

        else:
            file_list = queryset.filter(parent__isnull=True).order_by('-type')

        form = FolderModelForm(request, parent_object)
        return render(request, 'file.html',
                      {"form": form, "file_list": file_list, 'breadcrumb_trail': breadcrumb_trail,
                       'folder_object': parent_object})

    edit_object = None
    fid = request.POST.get('fid', '')
    if fid.isdecimal():
        edit_object = models.File.objects.filter(id=int(fid), project=request.current_user.project, type=2).first()

    if edit_object:

        form = FolderModelForm(request, parent_object, request.POST, instance=edit_object)
    else:
        form = FolderModelForm(request, parent_object, request.POST)

    if form.is_valid():
        form.instance.project = request.current_user.project
        form.instance.user = request.current_user.user
        form.instance.update_user = request.current_user.user
        form.instance.type = 2
        form.instance.parent = parent_object

        form.save()
        return JsonResponse({'status': True})

    return JsonResponse({'status': False, 'error': form.errors})


def file_delete(request, project_id):
    fid = request.GET.get('fid')
    # print(fid)

    delete_object = models.File.objects.filter(id=fid, project=request.current_user.project).first()

    if delete_object.type == "1":
        # 删除的是文件类型，需要删除数据库和cos上的文件

        print(request.current_user.project.used_space)
        print(delete_object.file_size)

        # 归还项目已使用空间
        request.current_user.project.used_space -= delete_object.file_size
        request.current_user.project.save()
        # 删除cos文件
        delete_file(request.current_user.project.bucket, request.current_user.project.region, delete_object.key)
        # 删除数据库中对应数据
        delete_object.delete()

        return JsonResponse({'status': True})

    # 删除文件夹，需要讲文件夹下所有文件和文件夹都一起删除，需要将文件使用的空间归还
    # total_size用于计算文件夹下所有文件的大小综合
    total_size = 0

    # folder_list用户统计要删除的文件夹下所有文件夹
    folder_list = [delete_object, ]

    # key_list
    key_list = []

    for folder in folder_list:
        child_list = models.File.objects.filter(parent=folder, project=request.current_user.project).order_by('-type')

        for child in child_list:

            if child.type == '2':
                # 文件类型为文件夹， 把文件夹都加入到folder_list，之后循环操作
                folder_list.append(child)
            else:
                # 文件类型为文件，total_size增加对应大小
                total_size += child.file_size
                key_list.append({'Key': child.key})

    # 如果key_list中有值，批量删除cos文件
    if key_list:
        delete_file_list(request.current_user.project.bucket, request.current_user.project.region, key_list)

    if total_size:
        request.current_user.project.used_space -= total_size
        request.current_user.project.save()

    # 数据库中删除对应文件夹，数据库默认级联删除，不用其他处理
    delete_object.delete()
    return JsonResponse({'status': True})


@csrf_exempt
def cos_credential(request, project_id):
    file_list = json.loads(request.body.decode('utf-8'))
    per_file_limit = request.current_user.price_package.file_size_limit * 1024 * 1024
    total_file_limit = request.current_user.price_package.space_limit * 1024 * 1024 * 1024
    total_size = 0
    for item in file_list:
        total_size += item['size']
        if item['size'] > per_file_limit:
            return JsonResponse({'status': False, 'error': f'<{item["name"]}>大小超出单个文件限制,建议您升级套餐'})

    if total_size + request.current_user.project.used_space > total_file_limit:
        return JsonResponse({'status': False, 'error': '文件总大小超出项目文件限制，建议您升级套餐'})

    data_dict = credential(request.current_user.project.bucket, request.current_user.project.region)
    return JsonResponse({'status': True, 'data': data_dict})


@csrf_exempt
def file_post(request, project_id):
    """
    name: fileName,
    file_size: fileSize,
    foler_id: {{ folder_object.id }},
    location: data.Location,
    key: key,
    etag: data.ETag,
    """

    form = FileModelForm(request, data=request.POST)
    print(request.POST)

    if form.is_valid():
        #
        # form.instance.project = request.current_user.project
        # form.instance.user = request.current_user.user
        # form.instance.update_user = request.current_user.user

        data_dict = form.cleaned_data
        data_dict.pop('etag')
        data_dict.update({"project": request.current_user.project,
                          'type': 1,
                          'user': request.current_user.user,
                          'update_user': request.current_user.user})
        instance = models.File.objects.create(**data_dict)

        request.current_user.project.used_space += data_dict['file_size']
        request.current_user.project.save()

        result = {
            "id": instance.id,
            "name": instance.name,
            'username': instance.update_user.username,
            'datetime': instance.update_time.strftime("%Y年%m月%d日 %H:%M"),
            'filesize': instance.file_size,
            'download_url': reverse('file_download',
                                    kwargs={"project_id": request.current_user.project.id, "file_id": instance.id})
        }
        # print(result)
        return JsonResponse({'status': True, "data": result})

    return JsonResponse({'status': False, 'error': form.errors})


def file_download(request, project_id, file_id):
    """文件下载"""
    file_object = models.File.objects.filter(id=file_id, project=request.current_user.project).first()

    res = requests.get(file_object.file_path)
    data = res.content

    response = HttpResponse(data)
    from django.utils.encoding import escape_uri_path
    # 设置响应头
    # response['Content-Disposition'] = "attachment; filename={}".format(file_object.name) # 中文下载乱码
    response['Content-Disposition'] = "attachment; filename={}".format(escape_uri_path(file_object.name))
    return response

    # # 文件分块处理（适用于大文件）
    # data = res.iter_content()
    #
    # # 设置content_type=application/octet-stream 用于提示下载框
    # response = HttpResponse(data, content_type="application/octet-stream")
    # from django.utils.encoding import escape_uri_path
    #
    # # 设置响应头：中文件文件名转义
    # response['Content-Disposition'] = "attachment; filename={};".format(escape_uri_path(file_object.name))
