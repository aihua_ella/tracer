# coding:utf-8
# @Author:ella

from django.shortcuts import render, redirect, HttpResponse
from web import models
from web.forms.project import CreateProjectForm
from django.http import JsonResponse
from utils.tencent.cos import create_bucket
import time


def project_list(request):
    if request.method == 'GET':
        form = CreateProjectForm(request)
        project_sort = {'star': [], 'my': [], 'join': []}

        my_project = models.Project.objects.filter(creator=request.current_user.user)

        for item in my_project:

            if item.star_target:
                project_sort['star'].append({'value': item, 'type': 'my'})
            else:
                project_sort['my'].append(item)

        join_project = models.ProjectUser.objects.filter(user=request.current_user.user)

        for item in join_project:

            if item.star:
                project_sort['star'].append({'value': item.project, 'type': 'join'})
            else:
                project_sort['join'].append(item.project)

        return render(request, 'project_list.html', {"form": form, "project_sort": project_sort})

    form = CreateProjectForm(request, request.POST)

    if form.is_valid():

        # 创建桶
        # title = form.cleaned_data['title']
        bucket = f'{request.current_user.user.mobile_phone}-{str(int(time.time()))}-1301482361'
        region = 'ap-chengdu'
        create_bucket(bucket, region)

        form.instance.bucket = bucket
        form.instance.region = region

        instance = form.save()
        # user_obj = models.UserInfo.objects.filter(pk=request.current_user.user.pk)
        request.current_user.user.increase_projectnum()

        # 创建项目时创建当前项目问题中的问题类型
        issues_type_list = []
        for item in models.IssuesType.PROJECT_INIT_LIST:
            issues_type_list.append(models.IssuesType(project=instance, title=item))
        models.IssuesType.objects.bulk_create(issues_type_list)

        return JsonResponse({'status': True})

    return JsonResponse({'status': False, 'error': form.errors})


def project_star(request, project_type, project_id):
    """
    星标项目
    :param request:
    :param project_type:
    :param project_id:
    :return:
    """
    if project_type == 'my':

        models.Project.objects.filter(creator=request.current_user.user, id=project_id).update(star_target=True)

        return redirect('project_list')

    elif project_type == 'join':

        models.ProjectUser.objects.filter(user=request.current_user.user, project_id=project_id).update(star=True)

        return redirect('project_list')

    return HttpResponse('请求错误')


def project_unstar(request, project_type, project_id):
    """
    取消星标项目
    :param request:
    :param project_type:
    :param project_id:
    :return:
    """
    if project_type == 'my':

        models.Project.objects.filter(creator=request.current_user.user, id=project_id).update(star_target=False)

        return redirect('project_list')

    elif project_type == 'join':

        models.ProjectUser.objects.filter(user=request.current_user.user, project_id=project_id).update(star=False)

        return redirect('project_list')

    return HttpResponse('请求错误')
