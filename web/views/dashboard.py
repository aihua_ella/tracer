# coding:utf-8
# @Author:ella

import collections
from django.shortcuts import render, redirect, HttpResponse
from web import models
from django.db.models import Count
from django.http import JsonResponse
import datetime, time


def dashboard(request, project_id):
    status_dict = collections.OrderedDict()

    for key, text in models.Issues.status_choices:
        status_dict[key] = {'text': text, 'count': 0}

    status_object_list = models.Issues.objects.filter(project_id=project_id).values('status').annotate(ct=Count('id'))
    for item in status_object_list:
        status_dict[item['status']]['count'] = item['ct']

    user_list = models.ProjectUser.objects.filter(project_id=project_id).values('user_id', 'user__username')

    top_ten_object = models.Issues.objects.filter(project_id=project_id, assign__isnull=False).order_by('-id')[0:10]
    # print(top_ten_object)

    context = {
        "status_dict": status_dict,
        "user_list": user_list,
        'top_ten_object': top_ten_object,
    }

    return render(request, 'dashboard.html', context)


def issues_chart(request, project_id):
    # 最近30天，每天创建的问题数量

    # print(123)

    today = datetime.datetime.now().date()

    data_dict = collections.OrderedDict()
    for i in range(0, 30):
        date = today - datetime.timedelta(days=i)
        data_dict[date.strftime('%Y-%m-%d')] = [time.mktime(date.timetuple()) * 1000, 0]

    # pymysql -->'ctime': "DATE_FORMAT(web_transaction.create_datetime,'%%Y-%%m-%%d')"
    result = models.Issues.objects.filter(project_id=project_id, create_datetime__gte=today - datetime.timedelta(days=30)).extra(
        select={'ctime': "strftime('%%Y-%%m-%%d',web_issues.create_datetime)"}).values('ctime').annotate(ct=Count('id'))

    for item in result:
        data_dict[item['ctime']][1] = item['ct']

    return JsonResponse({'status': True, 'data': list(data_dict.values())})

