# coding:utf-8
# @Author:ella

from django.shortcuts import render, reverse
from web.forms.issues import IssuesModelForm, IssuesReplyModelForm, InviteModelForm
from django.http import JsonResponse
from web import models
from utils.pagination import Pagination
from django.views.decorators.csrf import csrf_exempt
import json
from django.utils.safestring import mark_safe
from utils.encrypt import uid
import datetime


class CheckFilter:

    def __init__(self, name, data_list, request):
        self.name = name
        self.data_list = data_list
        self.request = request

    def __iter__(self):
        for item in self.data_list:
            key = str(item[0])
            text = item[1]
            ck = ''
            value_list = self.request.GET.getlist(self.name)
            if key in value_list:
                ck = 'checked'
                value_list.remove(key)
            else:
                value_list.append(key)

            query_dict = self.request.GET.copy()
            query_dict._mutable = True
            query_dict.setlist(self.name, value_list)

            if 'page' in query_dict:
                query_dict.pop('page')

            param_url = query_dict.urlencode()
            if param_url:
                url = "{}?{}".format(self.request.path_info, param_url)  # status=1&status=2&status=3&xx=1
            else:
                url = self.request.path_info

            html = f'<a class="cell" href="{url}"><input type="checkbox" {ck} /><label>{text}</label></a>'
            yield mark_safe(html)


class SelectFilter:

    def __init__(self, name, data_list, request):
        self.name = name
        self.data_list = data_list
        self.request = request

    def __iter__(self):
        yield mark_safe('<select class="select2" multiple="multiple" style="width:100%;">')
        for item in self.data_list:
            key = str(item[0])
            text = item[1]
            selected = ''
            value_list = self.request.GET.getlist(self.name)
            if key in value_list:
                selected = 'selected'
                value_list.remove(key)
            else:
                value_list.append(key)

            query_dict = self.request.GET.copy()
            query_dict._mutable = True
            query_dict.setlist(self.name, value_list)

            if 'page' in query_dict:
                query_dict.pop('page')

            param_url = query_dict.urlencode()
            if param_url:
                url = "{}?{}".format(self.request.path_info, param_url)  # status=1&status=2&status=3&xx=1
            else:
                url = self.request.path_info

            yield mark_safe(f"<option value='{url}' {selected} >{text}</option>")

            # html = f'<a class="cell" href="{url}"><input type="checkbox" {ck} /><label>{text}</label></a>'
            # yield mark_safe(html)

        yield mark_safe('</select>')


def issues(request, project_id):
    if request.method == 'GET':
        allow_filter_name = ['status', 'issues_type', 'priority', 'assign', 'attention']

        condition = {}
        for name in allow_filter_name:
            value_list = request.GET.getlist(name)

            if not value_list:
                continue
            condition[f'{name}__in'] = value_list

        queryset = models.Issues.objects.filter(project=request.current_user.project).order_by(
            '-create_datetime').filter(**condition)

        page_object = Pagination(
            current_page=request.GET.get('page'),
            all_count=queryset.count(),
            base_url=request.path_info,
            query_params=request.GET
        )

        issues_object_list = queryset[page_object.start:page_object.end]
        total_user_object = [(request.current_user.project.creator_id, request.current_user.project.creator.username), ]
        join_user_object = models.ProjectUser.objects.filter(project_id=project_id).values_list('user_id',
                                                                                                'user__username')
        total_user_object.extend(join_user_object)

        form = IssuesModelForm(request)
        issues_type_object = models.IssuesType.objects.filter(project_id=project_id).values_list('id', 'title')

        invite_form = InviteModelForm()

        context = {
            'form': form,
            'issues_object_list': issues_object_list,
            'page_html': page_object.page_html(),
            'filter_list': [
                {'title': '问题类型', 'filter': CheckFilter('issues_type', issues_type_object, request)},
                {'title': '状态', 'filter': CheckFilter('status', models.Issues.status_choices, request)},
                {'title': '优先级', 'filter': CheckFilter('priority', models.Issues.priority_choices, request)},
                {'title': '指派者', 'filter': SelectFilter('assign', total_user_object, request)},
                {'title': '关注者', 'filter': SelectFilter('attention', total_user_object, request)},
            ],
            "invite_form": invite_form,

        }

        return render(request, 'issues.html', context)

    form = IssuesModelForm(request, request.POST)

    if form.is_valid():
        form.instance.creator = request.current_user.user
        form.instance.project = request.current_user.project
        form.save()

        return JsonResponse({'status': True})

    return JsonResponse({'status': False, 'error': form.errors})


def issues_edit(request, project_id, issues_id):
    issues_object = models.Issues.objects.filter(id=issues_id).first()

    form = IssuesModelForm(request, instance=issues_object)

    return render(request, 'issues_edit.html', {"form": form, 'issues_object': issues_object})


@csrf_exempt
def issues_record(request, project_id, issues_id):
    if request.method == "GET":
        reply_list = models.IssuesReply.objects.filter(issues_id=issues_id,
                                                       issues__project=request.current_user.project)
        # 将queryset转换为json格式
        data_list = []
        for row in reply_list:
            data = {
                'id': row.id,
                'reply_type_text': row.get_reply_type_display(),
                'content': row.content,
                'creator': row.creator.username,
                'datetime': row.create_datetime.strftime("%Y-%m-%d %H:%M"),
                'parent_id': row.reply_id
            }
            data_list.append(data)

        return JsonResponse({'status': True, 'data': data_list})

    form = IssuesReplyModelForm(data=request.POST)
    if form.is_valid():
        form.instance.issues_id = issues_id
        form.instance.reply_type = 2
        form.instance.creator = request.current_user.user
        instance = form.save()
        info = {
            'id': instance.id,
            'reply_type_text': instance.get_reply_type_display(),
            'content': instance.content,
            'creator': instance.creator.username,
            'datetime': instance.create_datetime.strftime("%Y-%m-%d %H:%M"),
            'parent_id': instance.reply_id
        }

        return JsonResponse({'status': True, 'data': info})
    return JsonResponse({'status': False, 'error': form.errors})


@csrf_exempt
def issues_change(request, project_id, issues_id):
    issues_object = models.Issues.objects.filter(id=issues_id).first()

    def create_reply_record(content):
        # 如果检验全部通过，需要在IssuesReply表中新增一条操作记录
        instance = models.IssuesReply.objects.create(
            reply_type=1,
            issues=issues_object,
            content=change_record,
            creator=request.current_user.user
        )

        new_reply_dict = {
            'id': instance.id,
            'reply_type_text': instance.get_reply_type_display(),
            'content': instance.content,
            'creator': instance.creator.username,
            'datetime': instance.create_datetime.strftime("%Y-%m-%d %H:%M"),
            'parent_id': instance.reply_id
        }

        return new_reply_dict

    data_dict = json.loads(request.body.decode('utf-8'))
    name = data_dict.get('name')
    value = data_dict.get('value')
    field_object = models.Issues._meta.get_field(name)

    # 文本字段处理
    if name in ['subject', 'desc', 'start_date', 'end_date']:

        # 如果接收到的value为空，去数据库校验该字段是否可为空
        if not value:
            if not field_object.null:
                return JsonResponse({'status': False, 'error': '您选择的值不能为空'})

            setattr(issues_object, name, None)
            issues_object.save()
            change_record = f'{field_object.verbose_name}更新为空'

        else:
            setattr(issues_object, name, value)
            issues_object.save()
            change_record = f'{field_object.verbose_name}更新为{value}'

        return JsonResponse({'status': True, 'data': create_reply_record(change_record)})

    # ForeignKey字段处理
    if name in ['issues_type', 'module', 'parent', 'assign']:

        # 如果接收到的value为空，去数据库校验该字段是否可为空
        if not value:
            if not field_object.null:
                return JsonResponse({'status': False, 'error': '您选择的值不能为空'})

            setattr(issues_object, name, None)
            issues_object.save()
            change_record = f'{field_object.verbose_name}更新为空'

        else:
            if name == 'assign':
                # 判断用户是否是创建者或参与者
                if value == str(request.current_user.project.creator_id):
                    instance = request.current_user.project.creator
                else:
                    project_user_object = models.ProjectUser.objects.filter(project_id=project_id,
                                                                            user_id=value).first()

                    if project_user_object:
                        instance = project_user_object.user
                    else:
                        instance = None

                if not instance:
                    return JsonResponse({'status': False, 'error': '数据错误'})

                setattr(issues_object, name, instance)

                issues_object.save()
                change_record = f'{field_object.verbose_name}更新为{str(instance)}'

            else:
                instance = field_object.rel.model.objects.filter(id=value, project_id=project_id).first()

                if not instance:
                    return JsonResponse({'status': False, 'error': '您选择的值不存在'})

                setattr(issues_object, name, instance)

                issues_object.save()
                change_record = f'{field_object.verbose_name}更新为{str(instance)}'

        return JsonResponse({'status': True, 'data': create_reply_record(change_record)})

    # choices字段处理
    if name in ['priority', 'mode', 'status']:
        select_text = None

        for key, text in field_object.choices:

            if str(key) == value:
                select_text = text

        if not select_text:
            return JsonResponse({'status': False, 'error': '数据错误'})

        setattr(issues_object, name, value)
        issues_object.save()

        change_record = f'{field_object.verbose_name}更新为{select_text}'
        return JsonResponse({'status': True, 'data': create_reply_record(change_record)})

    # m2m字段处理

    if name == 'attention':

        if value and not isinstance(value, list):
            return JsonResponse({'status': False, 'error': '数据格式错误'})

        if not value:
            issues_object.attention.set([])
            issues_object.save()
            change_record = f'{field_object.verbose_name}更新为空'

        else:
            user_dict = {str(request.current_user.project.creator_id): request.current_user.project.creator.username}
            project_user_object = models.ProjectUser.objects.filter(project_id=project_id)

            for item in project_user_object:
                user_dict[str(item.user_id)] = item.user.username

            username_list = []

            for user_id in value:
                username = user_dict.get(str(user_id))

                if not username:
                    return JsonResponse({'status': False, 'error': '用户不存在，请刷新后重试'})
                username_list.append(username)

            issues_object.attention.set(value)
            issues_object.save()
            change_record = f'{field_object.verbose_name}更新为{",".join(username_list)}'

        return JsonResponse({'status': True, 'data': create_reply_record(change_record)})

    return JsonResponse({'status': False, 'error': '请求错误'})


def invite_url(request, project_id):
    form = InviteModelForm(request.POST)

    if form.is_valid():

        if request.current_user.user != request.current_user.project.creator:
            form.add_error('period', '只有创建者有权建立邀请链接')
            return JsonResponse({'status': False, 'error': form.errors})

        random_invite_code = uid(request.current_user.user.mobile_phone)
        form.instance.creator = request.current_user.user
        form.instance.project = request.current_user.project
        form.instance.code = random_invite_code
        form.save()

        # 将验邀请码返回给前端，前端页面上展示出来。
        url = "{scheme}://{host}{path}".format(
            scheme=request.scheme,
            host=request.get_host(),
            path=reverse('invite_join', kwargs={'code': random_invite_code})
        )

        return JsonResponse({'status': True, 'data': url})
        #
        # path = reverse('invite_join', kwargs={'code': random_invite_code})
        # url = f"{request.scheme}://{request.get_host()}{path}"
        #
        # return JsonResponse({'status': True, 'data': url})

    return JsonResponse({'status': False, 'error': form.errors})


def invite_join(request, code):
    """点击邀请链接进入页面"""
    invite_object = models.ProjectInvite.objects.filter(code=code).first()
    current_time = datetime.datetime.now()

    if not invite_object:
        return render(request, 'invite_join.html', {'error': "邀请码不存在"})

    if request.current_user.user == invite_object.project.creator:
        return render(request, 'invite_join.html', {'error': "项目创建者无需再加入"})

    exists = models.ProjectUser.objects.filter(user=request.current_user.user).exists()
    if exists:
        return render(request, 'invite_join.html', {'error': "已加入此项目，无需再加入"})

    # 项目成员数限制，以邀请者当前的套餐限制成员数

    max_transaction = models.OrderDetail.objects.filter(user=invite_object.project.creator).order_by(
        '-id').first()

    if max_transaction.price_policy.type == "1":
        # 免费版
        max_member = max_transaction.price_policy.member_limit

    else:
        # 缴费版
        if max_transaction.expire_time < current_time:
            # 已过期
            free_object = models.PricePackage.objects.filter(type=1).first()
            max_member = free_object.member_limit

        else:
            # 未过期
            max_member = max_transaction.price_policy.member_limit

    current_member = models.ProjectUser.objects.filter(project=invite_object.project).count()

    if current_member + 1 >= max_member:
        return render(request, 'invite_join.html', {'error': "项目人员已达上限"})

    limit_datetime = invite_object.create_datetime + datetime.timedelta(minutes=invite_object.period)
    if current_time > limit_datetime:
        return render(request, 'invite_join.html', {'error': '邀请码已过期'})

    # 数量限制？
    if invite_object.count:
        if invite_object.use_count >= invite_object.count:
            return render(request, 'invite_join.html', {'error': '邀请码数据已使用完'})
        invite_object.use_count += 1
        invite_object.save()

    models.ProjectUser.objects.create(user=request.current_user.user, project=invite_object.project)
    invite_object.project.member_num += 1
    invite_object.project.save()

    return render(request, 'invite_join.html', {'project': invite_object.project})
