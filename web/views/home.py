# coding:utf-8
# @Author:ella

from django.shortcuts import render, redirect, HttpResponse
from web import models
import datetime
from django_redis import get_redis_connection
import json
from utils.encrypt import uid
from utils.alipay import AliPay
from tracer import settings


def index(request):
    return render(request, 'index.html')


def price(request):
    price_package = models.PricePackage.objects.filter(type=2)

    return render(request, 'price.html', {"price_package": price_package})


def payment(request, package_id):
    policy_object = models.PricePackage.objects.filter(id=package_id, type=2).first()
    if not policy_object:
        return redirect('price')

    number = request.GET.get('number', "")
    if not number or not number.isdecimal():
        return redirect('price')

    number = int(number)
    if number < 1:
        return redirect('price')

    total_origin_price = number * policy_object.price  # 当前订单应付价格（已*num）

    balance = 0
    _object = None
    if request.current_user.price_package.type == '2':
        _object = models.OrderDetail.objects.filter(user=request.current_user.user, status=2).order_by('-id').first()
        total_timedelta = _object.expire_time - _object.start_time  # 总时长
        balance_timedelta = _object.expire_time - datetime.datetime.now()  # 剩余时长

        if total_timedelta.days == balance_timedelta.days:
            balance = _object.origin_price / total_timedelta.days * (balance_timedelta.days - 1)
        else:
            balance = _object.origin_price / total_timedelta.days * balance_timedelta.days

    if balance >= total_origin_price:
        return redirect('price')

    context = {
        'policy_id': policy_object.id,
        'number': number,
        'origin_price': total_origin_price,
        'balance': round(balance, 2),
        'total_price': total_origin_price - round(balance, 2)
    }

    conn = get_redis_connection()
    key = 'payment_{}'.format(request.current_user.user.mobile_phone)
    conn.set(key, json.dumps(context), ex=60 * 30)

    context['policy_object'] = policy_object
    context['transaction'] = _object
    print(context['transaction'])

    return render(request, 'payment.html', context)


def pay(request):
    conn = get_redis_connection()
    key = 'payment_{}'.format(request.current_user.user.mobile_phone)
    context_string = conn.get(key)
    if not context_string:
        return redirect('price')
    context = json.loads(context_string.decode('utf-8'))
    order_id = uid(request.current_user.user.mobile_phone)
    total_price = context['total_price']

    models.OrderDetail.objects.create(
        status=1,
        order_id=order_id,
        user=request.current_user.user,
        price_policy_id=context['policy_id'],
        num=context['number'],
        actually_paid=total_price,
        origin_price=context['origin_price']
    )
    # 生成支付链接

    ali_pay = AliPay(
        appid=settings.ALI_APPID,
        app_notify_url=settings.ALI_NOTIFY_URL,
        return_url=settings.ALI_RETURN_URL,
        app_private_key_path=settings.ALI_PRI_KEY_PATH,
        alipay_public_key_path=settings.ALI_PUB_KEY_PATH
    )
    query_params = ali_pay.direct_pay(
        subject="tracer payment",  # 商品简单描述
        out_trade_no=order_id,  # 商户订单号
        total_amount=total_price
    )
    pay_url = "{}?{}".format(settings.ALI_GATEWAY, query_params)
    return redirect(pay_url)


def pay_notify(request):
    """ 支付成功之后触发的URL """
    ali_pay = AliPay(
        appid=settings.ALI_APPID,
        app_notify_url=settings.ALI_NOTIFY_URL,
        return_url=settings.ALI_RETURN_URL,
        app_private_key_path=settings.ALI_PRI_KEY_PATH,
        alipay_public_key_path=settings.ALI_PUB_KEY_PATH
    )

    if request.method == 'GET':

        params = request.GET.dict()
        sign = params.pop('sign', None)
        status = ali_pay.verify(params, sign)
        # print(status)
        # print(params)

        if status:

            current_datetime = datetime.datetime.now()
            out_trade_no = params['out_trade_no']
            _object = models.OrderDetail.objects.filter(order_id=out_trade_no).first()

            _object.status = 2
            _object.start_time = current_datetime
            _object.expire_time = current_datetime + datetime.timedelta(days=365 * _object.num)
            _object.save()

            return render(request, 'pay_status.html')
        return render(request, 'pay_status.html', {'error': '支付失败'})

    else:
        from urllib.parse import parse_qs
        body_str = request.body.decode('utf-8')
        post_data = parse_qs(body_str)
        post_dict = {}
        for k, v in post_data.items():
            post_dict[k] = v[0]

        sign = post_dict.pop('sign', None)
        status = ali_pay.verify(post_dict, sign)
        if status:
            current_datetime = datetime.datetime.now()
            out_trade_no = post_dict['out_trade_no']
            _object = models.OrderDetail.objects.filter(order=out_trade_no).first()

            _object.status = 2
            _object.start_datetime = current_datetime
            _object.end_datetime = current_datetime + datetime.timedelta(days=365 * _object.count)
            _object.save()
            return HttpResponse('success')

        return HttpResponse('error')
