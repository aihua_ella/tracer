# coding:utf-8
# @Author:ella

from django.shortcuts import render, redirect, HttpResponse
from utils.tencent.cos import delete_bucket
from web import models


def setting(request, project_id):
    return render(request, 'setting.html')


def project_delete(request, project_id):
    if request.method == 'GET':
        return render(request, 'project_delete.html')

    project_name = request.POST.get('project_name')

    if not project_name or project_name != request.current_user.project.title:
        return render(request, 'project_delete.html', {'error': '项目名填写错误'})

    if request.current_user.user != request.current_user.project.creator:
        return render(request, 'project_delete.html', {'error': '只有项目创建者可删除项目，请联系项目管理员'})

    # 删除桶 + 删除数据库数据
    delete_bucket(request.current_user.project.bucket, request.current_user.project.region)
    print(1)
    models.Project.objects.filter(id=request.current_user.project.id).delete()
    print(2)

    return redirect('project_list')
