from django.db import models


class UserInfo(models.Model):
    """
    用户信息表
    """
    username = models.CharField(max_length=32, verbose_name='用户名', unique=True)
    email = models.EmailField(max_length=32, verbose_name='邮箱', unique=True)
    mobile_phone = models.CharField(max_length=32, verbose_name='手机号', unique=True)
    password = models.CharField(max_length=32, verbose_name='密码')
    project_num = models.SmallIntegerField(default=0, verbose_name='已创建项目数量')

    def __str__(self):
        return self.username

    def increase_projectnum(self):
        self.project_num += 1
        self.save(update_fields=['project_num'])


class OrderDetail(models.Model):
    """
    订单详情表
    """
    status_choices = (
        (1, '未支付'),
        (2, '已支付')
    )

    order_id = models.CharField(max_length=64, verbose_name='订单ID', unique=True)
    status = models.CharField(max_length=32, verbose_name='支付状态', choices=status_choices, default=2)
    user = models.ForeignKey('UserInfo', verbose_name='用户ID')
    price_policy = models.ForeignKey('PricePackage', verbose_name='购买套餐')
    num = models.IntegerField(verbose_name='购买数量', help_text='0表示无限期')
    origin_price = models.IntegerField(verbose_name='原价')
    actually_paid = models.IntegerField(verbose_name='实付价格')
    start_time = models.DateTimeField(null=True, blank=True)
    expire_time = models.DateTimeField(null=True, blank=True)
    create_time = models.DateTimeField(auto_now_add=True)


class PricePackage(models.Model):
    """
    价格套餐信息表
    """
    type_choices = (
        (1, '免费版'),
        (2, '收费版'),
        (3, '其他'),
    )

    type = models.CharField(max_length=32, choices=type_choices, verbose_name='套餐类型')
    title = models.CharField(max_length=32, verbose_name='标题')
    price = models.PositiveIntegerField(verbose_name='价格/年')
    project_limit = models.PositiveIntegerField(verbose_name='可创建项目数')
    member_limit = models.PositiveIntegerField(verbose_name='单个项目可参与人员数')
    space_limit = models.PositiveIntegerField(verbose_name='单个项目文件总大小', help_text='G')
    file_size_limit = models.PositiveIntegerField(verbose_name='单个文件大小', help_text='M')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')


class Project(models.Model):
    """
    项目信息表
    """
    COLOR_CHOICES = (
        (1, "#56b8eb"),  # 56b8eb
        (2, "#f28033"),  # f28033
        (3, "#ebc656"),  # ebc656
        (4, "#a2d148"),  # a2d148
        (5, "#20BFA4"),  # #20BFA4
        (6, "#7461c2"),  # 7461c2,
        (7, "#20bfa3"),  # 20bfa3,
    )

    title = models.CharField(max_length=32, verbose_name='项目名称')
    color = models.SmallIntegerField(choices=COLOR_CHOICES, verbose_name='颜色', default=1)
    star_target = models.BooleanField(default=False, verbose_name='星标')
    member_num = models.IntegerField(default=1, verbose_name='成员数量')
    creator = models.ForeignKey('UserInfo', verbose_name='创建者', related_name='user')
    used_space = models.IntegerField(default=0, verbose_name='项目已用空间/KB')
    info = models.TextField(verbose_name='项目描述')
    bucket = models.CharField(max_length=128, verbose_name='cos桶')
    region = models.CharField(max_length=32, verbose_name='cos区域')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')

    def __str__(self):
        return self.title


class ProjectUser(models.Model):
    """ 项目参与者 """
    user = models.ForeignKey(verbose_name='参与者', to='UserInfo')
    project = models.ForeignKey(verbose_name='项目', to='Project')
    star = models.BooleanField(verbose_name='星标', default=False)
    create_datetime = models.DateTimeField(verbose_name='加入时间', auto_now_add=True)


class Wiki(models.Model):
    """wiki文档相关"""
    user = models.ForeignKey(verbose_name='创建者', to='UserInfo')
    project = models.ForeignKey(verbose_name='项目', to='Project')
    title = models.CharField(max_length=64, verbose_name='标题')
    content = models.TextField(verbose_name='内容')
    parent = models.ForeignKey(verbose_name='父文档', to='Wiki', null=True, blank=True, related_name='children')
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True, null=True, blank=True)
    depth = models.IntegerField(verbose_name='深度', default=1)

    def __str__(self):
        return self.title


class File(models.Model):
    """文件相关"""

    type_choices = (
        (1, '文件'),
        (2, '文件夹'),
    )

    name = models.CharField(verbose_name='文件名', max_length=32)
    project = models.ForeignKey(verbose_name='项目', to='Project')
    type = models.CharField(verbose_name='文件类型', max_length=32, choices=type_choices, default=2)
    file_size = models.IntegerField(verbose_name='文件大小', null=True, blank=True)
    file_path = models.CharField(verbose_name='文件路径', null=True, blank=True, max_length=255)
    user = models.ForeignKey(verbose_name='创建者', to='UserInfo', related_name='creator')
    update_user = models.ForeignKey(verbose_name='更新者', to='UserInfo')
    parent = models.ForeignKey(verbose_name='上级目录', to='File', null=True, blank=True, related_name='children')
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True, null=True, blank=True)
    key = models.CharField(verbose_name='key', max_length=128, null=True, blank=True)


class Issues(models.Model):
    """ 问题 """
    project = models.ForeignKey(verbose_name='项目', to='Project')
    issues_type = models.ForeignKey(verbose_name='问题类型', to='IssuesType')
    module = models.ForeignKey(verbose_name='模块', to='Module', null=True, blank=True)

    subject = models.CharField(verbose_name='主题', max_length=80)
    desc = models.TextField(verbose_name='问题描述')
    priority_choices = (
        ("danger", "高"),
        ("warning", "中"),
        ("success", "低"),
    )
    priority = models.CharField(verbose_name='优先级', max_length=12, choices=priority_choices, default='danger')

    # 新建、处理中、已解决、已忽略、待反馈、已关闭、重新打开
    status_choices = (
        (1, '新建'),
        (2, '处理中'),
        (3, '已解决'),
        (4, '已忽略'),
        (5, '待反馈'),
        (6, '已关闭'),
        (7, '重新打开'),
    )
    status = models.SmallIntegerField(verbose_name='状态', choices=status_choices, default=1)

    assign = models.ForeignKey(verbose_name='指派', to='UserInfo', related_name='task', null=True, blank=True)
    attention = models.ManyToManyField(verbose_name='关注者', to='UserInfo', related_name='observe', blank=True)
    start_date = models.DateField(verbose_name='开始时间', null=True, blank=True)
    end_date = models.DateField(verbose_name='结束时间', null=True, blank=True)
    mode_choices = (
        (1, '公开模式'),
        (2, '隐私模式'),
    )
    mode = models.SmallIntegerField(verbose_name='模式', choices=mode_choices, default=1)

    parent = models.ForeignKey(verbose_name='父问题', to='self', related_name='child', null=True, blank=True,
                               on_delete=models.SET_NULL)

    creator = models.ForeignKey(verbose_name='创建者', to='UserInfo', related_name='create_problems')

    create_datetime = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    latest_update_datetime = models.DateTimeField(verbose_name='最后更新时间', auto_now=True)

    def __str__(self):
        return self.subject


class Module(models.Model):
    """ 模块（里程碑）"""
    project = models.ForeignKey(verbose_name='项目', to='Project')
    title = models.CharField(verbose_name='模块名称', max_length=32)

    def __str__(self):
        return self.title


class IssuesType(models.Model):
    """ 问题类型 例如：任务、功能、Bug """

    PROJECT_INIT_LIST = ["任务", '功能', 'Bug']

    title = models.CharField(verbose_name='类型名称', max_length=32)
    project = models.ForeignKey(verbose_name='项目', to='Project')

    def __str__(self):
        return self.title


class IssuesReply(models.Model):
    """ 问题回复"""

    reply_type_choices = (
        (1, '修改记录'),
        (2, '回复')
    )
    reply_type = models.IntegerField(verbose_name='类型', choices=reply_type_choices)

    issues = models.ForeignKey(verbose_name='问题', to='Issues')
    content = models.TextField(verbose_name='描述')
    creator = models.ForeignKey(verbose_name='创建者', to='UserInfo', related_name='create_reply')
    create_datetime = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)

    reply = models.ForeignKey(verbose_name='回复', to='self', null=True, blank=True)


class ProjectInvite(models.Model):
    """ 项目邀请码 """
    project = models.ForeignKey(verbose_name='项目', to='Project')
    code = models.CharField(verbose_name='邀请码', max_length=64, unique=True)
    count = models.PositiveIntegerField(verbose_name='限制数量', null=True, blank=True, help_text='空表示无数量限制')
    use_count = models.PositiveIntegerField(verbose_name='已邀请数量', default=0)
    period_choices = (
        (30, '30分钟'),
        (60, '1小时'),
        (300, '5小时'),
        (1440, '24小时'),
    )
    period = models.IntegerField(verbose_name='有效期', choices=period_choices, default=1440)
    create_datetime = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    creator = models.ForeignKey(verbose_name='创建者', to='UserInfo', related_name='create_invite')
