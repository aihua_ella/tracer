# coding:utf-8
# @Author:ella
from django.template import Library
from django.urls import reverse

from web import models

register = Library()


@register.inclusion_tag('inclusion/all_project_list.html')
def all_project_list(request):
    my_projects = models.Project.objects.filter(creator=request.current_user.user)

    join_projects = models.ProjectUser.objects.filter(user=request.current_user.user)

    return {'my': my_projects, "join": join_projects, 'request': request}


@register.inclusion_tag('inclusion/project_menu_list.html')
def project_menu_list(request):
    data_list = [
        {'title': '概览', 'url': reverse('dashboard', kwargs={"project_id": request.current_user.project.id})},
        {'title': '问题', 'url': reverse('issues', kwargs={"project_id": request.current_user.project.id})},
        {'title': '统计', 'url': reverse('statistics', kwargs={"project_id": request.current_user.project.id})},
        {'title': 'wiki', 'url': reverse('wiki', kwargs={"project_id": request.current_user.project.id})},
        {'title': '文件', 'url': reverse('file', kwargs={"project_id": request.current_user.project.id})},
        {'title': '配置', 'url': reverse('setting', kwargs={"project_id": request.current_user.project.id})}
    ]

    for item in data_list:

        if request.path_info.startswith(item['url']):
            item['class'] = 'active'

    return {'data_list': data_list}
