# coding:utf-8
# @Author:ella


from django.utils.deprecation import MiddlewareMixin
from web import models
from django.shortcuts import redirect
from django.conf import settings
import datetime
from django.shortcuts import redirect, reverse


class Tracer(object):

    def __init__(self):
        self.user = None
        self.price_package = None
        self.project = None


class AuthMiddleware(MiddlewareMixin):
    def process_request(self, request):
        """
        如果用户已登录，获取request的值
        :param request:
        :return:
        """
        request.current_user = Tracer()
        user_id = request.session.get('user_id', 0)
        user_object = models.UserInfo.objects.filter(pk=user_id).first()

        request.current_user.user = user_object

        if request.path_info in settings.WHITE_LIST:
            return

        if not request.current_user.user:
            return redirect('login')

        # 获取当前用户ID值最大（最近交易记录）
        _object = models.OrderDetail.objects.filter(user=user_object, status=2).order_by('-id').first()
        # 判断是否已过期
        current_datetime = datetime.datetime.now()
        if _object.expire_time and _object.expire_time < current_datetime:
            _object = models.OrderDetail.objects.filter(user=user_object, status=2,
                                                        price_policy__category=1).first()

            request.tracer.price_policy = _object.price_policy

        request.current_user.price_package = _object.price_policy

    def process_view(self, request, view, arg, kwargs):
        """
        校验用户要进入的项目是否是自己创建或参与
        :param request:
        :return:
        """

        # 判断URL是否是以manage开头，如果是则判断项目ID是否是我创建 or 参与

        if not request.path_info.startswith('/manage/'):
            return

        project_id = kwargs.get('project_id')

        my_project = models.Project.objects.filter(id=project_id).first()

        if my_project:
            request.current_user.project = my_project
            return

        join_project = models.ProjectUser.objects.filter(project_id=project_id).first()

        if join_project:
            request.current_user.project = join_project.project
            return

        return redirect('project_list')
