from django.conf.urls import url, include

from web.views import account, home, project, statistics, wiki, file, setting, issues, dashboard

urlpatterns = [
    url(r'^register/$', account.register, name="register"),
    url(r'^login/sms/$', account.login_sms, name="login_sms"),
    url(r'^login/$', account.login, name="login"),
    url(r'^logout/$', account.logout, name="logout"),
    url(r'^send_sms/$', account.send_sms, name="send_sms"),
    url(r'^image_code/$', account.image_code, name="image_code"),
    url(r'^index/$', home.index, name="index"),
    url(r'^price/$', home.price, name="price"),
    url(r'^payment/(?P<package_id>\d+)/$', home.payment, name="payment"),
    url(r'^pay/$', home.pay, name='pay'),
    url(r'^pay/notify/$', home.pay_notify, name='pay_policy'),

    # url(r'^cos/credential/$', file.cos_credential, name='file'),
    url(r'^project/list/$', project.project_list, name="project_list"),

    url(r'^project/star/(?P<project_type>\w+)/(?P<project_id>\w+)/$', project.project_star, name="project_star"),
    url(r'^project/unstar/(?P<project_type>\w+)/(?P<project_id>\w+)/$', project.project_unstar, name="project_unstar"),

    url(r'^manage/(?P<project_id>\w+)/', include([

        url(r'^statistics/$', statistics.statistics, name='statistics'),
        url(r'^statistics/priority/$', statistics.statistics_priority, name='statistics_priority'),
        url(r'^statistics/project/user/$', statistics.statistics_project_user, name='statistics_project_user'),

        url(r'^file/$', file.file, name='file'),
        url(r'^file/delete$', file.file_delete, name='file_delete'),
        url(r'^file/post', file.file_post, name='file_post'),
        url(r'^file/download/(?P<file_id>\d+)', file.file_download, name='file_download'),
        url(r'^cos/credential$', file.cos_credential, name='cos_credential'),

        url(r'^wiki/$', wiki.wiki, name='wiki'),
        url(r'^wiki/add/$', wiki.wiki_add, name='wiki_add'),
        url(r'^wiki/edit/(?P<wiki_id>\d+)$', wiki.wiki_edit, name='wiki_edit'),
        url(r'^wiki/upload/$', wiki.wiki_upload, name='wiki_upload'),
        url(r'^wiki/del/(?P<wiki_id>\d+)$', wiki.wiki_del, name='wiki_del'),
        url(r'^wiki/catalog/$', wiki.wiki_catalog, name='wiki_catalog'),

        url(r'^setting/$', setting.setting, name='setting'),
        url(r'^setting/project_delete/$', setting.project_delete, name='project_delete'),

        url(r'^issues/$', issues.issues, name='issues'),
        url(r'^issues/edit/(?P<issues_id>\d+)$', issues.issues_edit, name='issues_edit'),
        url(r'^issues/record/(?P<issues_id>\d+)/$', issues.issues_record, name='issues_record'),
        url(r'^issues/change/(?P<issues_id>\d+)/$', issues.issues_change, name='issues_change'),
        url(r'^issues/invite/url/$', issues.invite_url, name='invite_url'),

        url(r'^dashboard/$', dashboard.dashboard, name='dashboard'),
        url(r'^dashboard/issues/chart$', dashboard.issues_chart, name='issues_chart'),

    ])),
    url(r'^invite/join/(?P<code>\w+)/$', issues.invite_join, name="invite_join"),
]
