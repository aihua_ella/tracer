# coding:utf-8
# @Author:ella
from web.forms.bootstrapForm import BootstrapForm
from django import forms
from web import models


class WikiModelForm(BootstrapForm, forms.ModelForm):
    class Meta:
        model = models.Wiki
        fields = ['title', 'content', 'parent']

    def __init__(self, request, *args, **kwars):
        super().__init__(*args, **kwars)

        total_data_list = [("", '请选择上级文档'), ]

        data_list = models.Wiki.objects.filter(project=request.current_user.project).values_list('id', 'title')
        total_data_list.extend(data_list)

        self.fields['parent'].choices = total_data_list
