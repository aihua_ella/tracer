# coding:utf-8
# @Author:ella


from django import forms
from web.forms.bootstrapForm import BootstrapForm
from web import models
from django.core.validators import ValidationError
from utils.tencent.cos import check_file


class FolderModelForm(BootstrapForm, forms.ModelForm):
    class Meta:
        model = models.File
        fields = ['name']

    def __init__(self, request, parent_object, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request
        self.parent_object = parent_object

    def clean_name(self):
        name = self.cleaned_data['name']

        # 检测当前目录下有没有重名的文件夹
        quertset = models.File.objects.filter(name=name, project=self.request.current_user.project, type=2)

        if self.parent_object:
            exists = quertset.filter(parent=self.parent_object).exists()

        else:
            exists = quertset.filter(parent__isnull=True).exists()

        if exists:
            raise ValidationError('文件夹已存在')

        return name


class FileModelForm(BootstrapForm, forms.ModelForm):
    etag = forms.CharField(label='ETag')

    class Meta:
        model = models.File
        exclude = ['user', 'update_user', 'project', 'type', 'update_datetime', 'create_time']

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

    def clean_file_path(self):
        file_path = self.cleaned_data['file_path']

        return f"https://{file_path}"


    """
    def clean(self):
        etag = self.cleaned_data['etag']
        key = self.cleaned_data['key']
        size = self.cleaned_data['file_size']

        if not key or not etag:
            return self.cleaned_data

        # 向COS校验文件是否合法
        from qcloud_cos.cos_exception import CosServiceError

        try:
            result = check_file(self.request.current_user.projrct.bucket, self.request.current_user.projrct.region, key)

        except CosServiceError as e:
            self.add_error('key', '文件不存在')
            return self.cleaned_data

        cos_etag = result.get('ETag')
        if cos_etag != etag:
            self.add_error('etag', 'ETag错误')

        if size != result.get('Content-Length'):
            self.add_error('size', '文件大小不一致')

        return self.cleaned_data
        
        """

