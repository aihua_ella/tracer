# coding:utf-8
# @Author:ella

from django import forms
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from web import models
from django.conf import settings
import random
from utils.tencent.sms import send_sms_single
from django_redis import get_redis_connection
from utils import encrypt
import re
from web.forms.bootstrapForm import BootstrapForm


class RegisterModelForm(BootstrapForm, forms.ModelForm):
    username = forms.CharField(label='用户名', max_length=8)
    mobile_phone = forms.CharField(label='手机号', validators=[RegexValidator(r'^(1[3|4|5|6|7|8|9])\d{9}$', '手机号格式错误'), ])
    password = forms.CharField(
        label='密码',
        min_length=8,
        max_length=64,
        error_messages={
            'min_length': "密码长度不能小于8个字符",
            'max_length': "密码长度不能大于64个字符"
        },
        widget=forms.PasswordInput()
    )

    confirm_password = forms.CharField(
        label='重复密码',
        min_length=8,
        max_length=64,
        error_messages={
            'min_length': "密码长度不能小于8个字符",
            'max_length': "密码长度不能大于64个字符"
        },
        widget=forms.PasswordInput()
    )

    code = forms.CharField(
        label='验证码',
        widget=forms.TextInput())

    class Meta:
        model = models.UserInfo
        fields = ['username', 'email', 'password', 'confirm_password', 'mobile_phone', 'code']

    def clean_username(self):
        username = self.cleaned_data['username']
        name_rule = r'[a-zA-Z0-9_-]{4,16}$'

        if not re.match(name_rule, username):
            raise ValidationError('用户名需为4-16为的字母、数字、下划线、减号')

        exists = models.UserInfo.objects.filter(username=username).exists()

        if exists:
            raise ValidationError('用户名已存在，请重新输入')

        return username

    def clean_email(self):
        email = self.cleaned_data['email']

        exists = models.UserInfo.objects.filter(email=email).exists()

        if exists:
            raise ValidationError('邮箱已注册，请重新输入')

        return email

    def clean_password(self):
        password = self.cleaned_data['password']
        # print(self.cleaned_data)

        password_rule = r'^.*(?=.{6,})(?=.*\d)(?=.*[A-Za-z]).*$'

        if not re.match(password_rule, password):
            raise ValidationError('密码最少6位，包括大写字母，小写字母，数字')

        return encrypt.md5(password)

    def clean_confirm_password(self):
        # print(self.cleaned_data)
        # password = self.cleaned_data['password']
        password = self.cleaned_data.get('password')
        confirm_password = encrypt.md5(self.cleaned_data['confirm_password'])

        if not password:
            return confirm_password

        # password_rule = r'^.*(?=.{6,})(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*? ]).*$'
        password_rule = r'^.*(?=.{6,})(?=.*\d)(?=.*[A-Za-z]).*$'

        if not re.match(password_rule, password):
            raise ValidationError('密码最少6位，包括大写字母，小写字母，数字')

        if not password == confirm_password:
            raise ValidationError('两次密码不一致')

        return confirm_password

    def clean_mobile_phone(self):
        mobile_phone = self.cleaned_data['mobile_phone']

        exists = models.UserInfo.objects.filter(mobile_phone=mobile_phone).exists()

        if exists:

            # self.add_error('mobile_phone', '手机号已注册')
            raise ValidationError('手机号已注册')

        return mobile_phone

    def clean_code(self):
        code = self.cleaned_data['code']
        # code = self.cleaned_data.get('code')
        # mobile_phone = self.cleaned_data['mobile_phone']
        mobile_phone = self.cleaned_data.get('mobile_phone')
        if not mobile_phone:
            return code

        conn = get_redis_connection()
        redis_code = conn.get(mobile_phone)

        if not redis_code:
            raise ValidationError('验证码失效或未发送，请重新发送')

        redis_str_code = conn.get(mobile_phone).decode('utf-8')

        if code.strip() != redis_str_code:
            raise ValidationError('验证码错误，请重新输入')

        return code

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.widget.attrs['placeholder'] = '请输入%s' % (field.label,)


class SmsLoginForm(BootstrapForm, forms.Form):
    mobile_phone = forms.CharField(label='手机号', validators=[RegexValidator(r'^(1[3|4|5|6|7|8|9])\d{9}$', '手机号格式错误'), ])

    code = forms.CharField(
        label='验证码',
        widget=forms.TextInput())

    def clean_phone(self):
        mobile_phone = self.cleaned_data['mobile_phone']

        exists = models.UserInfo.objects.filter(mobile_phone=mobile_phone).exists()

        if not exists:
            raise ValidationError('手机号不存在')

        return mobile_phone

    def clean_code(self):

        code = self.cleaned_data['code']
        # mobile_phone = self.cleaned_data['mobile_phone']
        mobile_phone = self.cleaned_data.get('mobile_phone')
        if not mobile_phone:
            return code

        conn = get_redis_connection()
        redis_code = conn.get(mobile_phone)
        print(f'登录码{redis_code}')

        if not redis_code:
            raise ValidationError('验证码失效或未发送，请重新发送')

        redis_str_code = conn.get(mobile_phone).decode('utf-8')

        if code.strip() != redis_str_code:
            raise ValidationError('验证码错误，请重新输入')

        return code


class SendSmsForm(forms.Form):
    mobile_phone = forms.CharField(label='手机号', validators=[RegexValidator(r'^(1[3|4|5|6|7|8|9])\d{9}$', '手机号格式错误'), ])

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

    def clean_mobile_phone(self):
        mobile_phone = self.cleaned_data['mobile_phone']

        # 校验短信模板

        tpl = self.request.GET.get('tpl')
        template_id = settings.TENCENT_SMS_TEMPLATES[tpl]

        if not template_id:
            raise ValidationError('模板不存在')

        exists = models.UserInfo.objects.filter(mobile_phone=mobile_phone).exists()

        if tpl == 'login':
            # 校验手机号
            if not exists:
                raise ValidationError('手机号不存在')

        else:
            if exists:
                raise ValidationError('手机号已存在')

        code = random.randrange(1000, 9999)

        # 发送短信
        sms = send_sms_single(mobile_phone, template_id, [code, ])
        if sms['result'] != 0:
            raise ValidationError('短信发送失败，{}'.format(sms['errmsg']))

        # 验证码 写入redis（django-redis）
        conn = get_redis_connection()
        conn.set(mobile_phone, code, ex=60)
        print(f'注册码{code}')

        return mobile_phone


class LoginForm(BootstrapForm, forms.Form):
    username = forms.CharField(label='手机号或邮箱')

    password = forms.CharField(label='密码', widget=forms.PasswordInput(render_value=True))
    code = forms.CharField(label='验证码')

    def clean_password(self):
        password = self.cleaned_data['password']

        return encrypt.md5(password)

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

    def clean_code(self):
        """
        :return: 验证图片验证码
        """

        code = self.cleaned_data['code']

        session_code = self.request.session.get('image_code')

        if not session_code:
            raise ValidationError('验证码已失效，请重新获取')

        if code != session_code:
            raise ValidationError('验证码输入错误，请重新输入')

        return code
