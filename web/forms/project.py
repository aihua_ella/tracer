# coding:utf-8
# @Author:ella


from django import forms
from web import models
from web.forms.bootstrapForm import BootstrapForm
from django.core.validators import ValidationError
from .widgets import ColorRadioSelect


class CreateProjectForm(BootstrapForm, forms.ModelForm):
    bootstrap_exclude_class = ['color', ]

    class Meta:
        model = models.Project
        fields = ['title', 'color', 'info']
        widgets = {
            'color': ColorRadioSelect(attrs={'class': 'color-radio'})
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.request = request

    def clean(self):
        self.instance.creator = self.request.current_user.user

        return self.cleaned_data

    def clean_title(self):
        title = self.cleaned_data['title']

        exists = models.Project.objects.filter(title=title).first()

        if exists:
            raise ValidationError('与现有项目重名, 请修改')

        # 已经创建的项目个数
        project_num = self.request.current_user.user.project_num

        # 用户所在套餐总可创建项目个数
        limit_num = self.request.current_user.price_package.project_limit

        if project_num >= limit_num:
            raise ValidationError(f'您最多可创建{limit_num}个项目，建议您升级套餐')
        return title










