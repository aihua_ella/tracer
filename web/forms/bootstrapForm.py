# coding:utf-8
# @Author:ella


class BootstrapForm(object):
    bootstrap_exclude_class = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():

            if name in self.bootstrap_exclude_class:
                continue

            old_class = field.widget.attrs.get('class', '')

            field.widget.attrs['class'] = f'{old_class} form-control'
            field.widget.attrs['placeholder'] = '请输入%s' % (field.label,)

