# coding:utf-8
# @Author:ella


""" 创建桶 """
from qcloud_cos import CosConfig
from qcloud_cos import CosS3Client
import sys
import logging
from tracer import settings

logging.basicConfig(level=logging.INFO, stream=sys.stdout)

secret_id = settings.TENCENT_COS_SECRET_ID  # 替换为用户的 secretId
secret_key = settings.TENCENT_COS_SECRET_KEY


region = 'ap-chengdu'     # 替换为用户的 Region
token = None                # 使用临时密钥需要传入 Token，默认为空，可不填
scheme = 'https'            # 指定使用 http/https 协议来访问 COS，默认为 https，可不填
config = CosConfig(Region=region, SecretId=secret_id, SecretKey=secret_key, Token=token, Scheme=scheme)
# 2. 获取客户端对象
client = CosS3Client(config)


response = client.create_bucket(
    Bucket='wubaobao-1301482361'
)

