# coding:utf-8
# @Author:ella


""" 上传文件 """
from qcloud_cos import CosConfig
from qcloud_cos import CosS3Client
import sys
import logging
from tracer import settings

# logging.basicConfig(level=logging.INFO, stream=sys.stdout)

# secret_id = settings.TENCENT_COS_SECRET_ID  # 替换为用户的 secretId
# secret_key = settings.TENCENT_COS_SECRET_KEY


region = 'ap-chengdu'  # 替换为用户的 Region
# token = None                # 使用临时密钥需要传入 Token，默认为空，可不填
# scheme = 'https'            # 指定使用 http/https 协议来访问 COS，默认为 https，可不填
config = CosConfig(Region=region, SecretId=settings.TENCENT_COS_SECRET_ID, SecretKey=settings.TENCENT_COS_SECRET_KEY)
# 2. 获取客户端对象
client = CosS3Client(config)

# 单个删除
# response = client.delete_object(
#     Bucket='thefirst-18201048114-1585046638-1301482361',
#     Key='1111111.png',  # 上传之后的文件名称
#
# )


# 批量删除

objects = {
    "Quiet": "true",
    "Object": [
        {
            "Key": "小米CC9e.jpg"
        },
        {
            "Key": "350.jpeg"
        }
    ]
}

client.delete_objects(
    Bucket='thefirst-18201048114-1585046638-1301482361',
    Delete=objects
)
