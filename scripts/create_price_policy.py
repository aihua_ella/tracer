#!/usr/bin/env python
# -*- coding:utf-8 -*-
from scripts import base
from web import models


def run():
    models.PricePackage.objects.create(
        title='VIP',
        price=100,
        project_limit=50,
        member_limit=10,
        space_limit=100,
        file_size_limit=500,
        type=2
    )

    models.PricePackage.objects.create(
        title='SVIP',
        price=200,
        project_limit=150,
        member_limit=110,
        space_limit=100,
        file_size_limit=1024,
        type=2
    )

    models.PricePackage.objects.create(
        title='SSVIP',
        price=500,
        project_limit=550,
        member_limit=510,
        space_limit=510,
        file_size_limit=2048,
        type=2
    )


if __name__ == '__main__':
    run()
