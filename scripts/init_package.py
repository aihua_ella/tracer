# coding:utf-8
# @Author:ella

from scripts import base
from web import models


# 往数据库添加数据：链接数据库、操作、关闭链接


def run():
    exists = models.PricePackage.objects.filter(type=1, title='免费版').exists()

    if not exists:
        models.PricePackage.objects.create(
            title='免费版',
            type=1,
            price=0,
            project_limit=3,
            member_limit=2,
            space_limit=20,
            file_size_limit=5
        )


if __name__ == "__main__":

    run()